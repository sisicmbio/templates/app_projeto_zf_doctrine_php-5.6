#!/usr/bin/env bash
mkdir -p /var/www/html/{projeto}/data/cache
mkdir -p /var/www/html/{projeto}/data/proxy_cache
mkdir -p /var/www/html/{projeto}/data/file/pdf
mkdir -p /var/www/html/{projeto}/data/upload
echo "###################################################"
echo "Atribuindo permissoes padrao ao sistema {projeto} ... "
echo "###################################################"
chown root:apache /var/www/html/{projeto} -R
chmod 750 /var/www/html/{projeto} -R
chmod 775 /var/www/html/{projeto}/data -R
chmod 775 /var/www/html/{projeto}/data/cache -R
chmod 775 /var/www/html/{projeto}/data/proxy_cache -R
chmod 775 /var/www/html/{projeto}/data/file/pdf -R
chmod 775 /var/www/html/{projeto}/data/upload -R


#    echo -n $"Atribuindo permissoes padrao ao sistema Sarr ... "
#    /bin/chown root:apache /var/www/html/sarr -R
#    /bin/rm -rf /var/www/html/sarr/data
#    /bin/ln -s  /var/www/html/arquivos_extras/sarr/data /var/www/html/sarr/
#    /usr/bin/find /var/www/html/sarr -type d -exec /bin/chmod 750 {} \;
#    /usr/bin/find /var/www/html/sarr -type f -exec /bin/chmod 640 {} \;
#    /bin/chmod 770 /var/www/html/sarr/data/cache -R
#    /bin/chmod 770 /var/www/html/sarr/data/proxy_cache -R
#    /bin/chmod 770 /var/www/html/sarr/data/file/gru -R
#    /bin/chmod 770 /var/www/html/sarr/data/Gp-A-SARR-bsa -R
#    /bin/chmod 770 /var/www/html/sarr/data/proxy_cache -R
#    /bin/chmod 770 /var/www/html/sarr/data/Gp-A-SARR-bsa_bkp -R
#    /bin/chmod 770 /var/www/html/sarr/data/file/codbarras -R
#    /bin/chmod 770 /var/www/html/sarr/data/file/pdf -R
#    /bin/chmod 770 /var/www/html/sarr/data/logs/controle-debito -R
#    /bin/chmod 770 /var/www/html/sarr/jobs/robot
#    /bin/chmod 770 /var/www/html/sarr/data/ -R
