#!/bin/bash

# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
#rm -rf /run/httpd/* /tmp/httpd*

export > tmp.export
sed -i "s|declare -x |Define |" tmp.export
sed -i "s|=| |" tmp.export
sed -i "s|\"||" tmp.export
sed -i "s|\"||" tmp.export
cat tmp.export | grep PDCI_ | awk -F " " ' { print $1 " " $2 " " $3 }' > /etc/httpd/conf.d/pdci_app_httpd.env
rm -f tmp.export

#pdci_ip_container=$(ifconfig | awk '/inet /{print $2}' | grep 172.)
#echo "pdci_ip_container => " ${pdci_ip_container}

sed -i "s|#ServerName www.example.com:80|ServerName ${VIRTUAL_HOST}:80|"  /etc/httpd/conf/httpd.conf

postfix start

exec /usr/sbin/apachectl -DFOREGROUND


 #docker build -f dockerfile.yml -t app_lafsisbio .
